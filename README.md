# test

# Task 1
```sh
cat access.log | awk '{ print $1 }' | sort -n | uniq > uniq_ip
```
# Task 3
Можно чистить с помощью [api](https://docs.gitlab.com/ee/api/container_registry.html). 
Реализовывал подобное для Harbor Registry - https://gitlab.com/kudryashovav/harbor/blob/master/cleanup.md
Сдесь можно реализовать так же.
# Task 4
Answer: `lvm` 
```sh
[root@test]# vgdisplay
  --- Volume group ---
  VG Name               vgsys
  System ID             
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  8
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                7
  Open LV               7
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <49,50 GiB
  PE Size               4,00 MiB
  Total PE              12671
  Alloc PE / Size       7936 / 31,00 GiB
  Free  PE / Size       4735 / <18,50 GiB
  VG UUID               nq9Ani-5rVb-2uPD-D3Jp-gVEX-sTpp-OjUnvm

[root@test]# lvextend -l +4735 /dev/vg_sys/var_lib_docker
[root@test]# resize2fs /dev/vg_sys/var_lib_docker
```
# Task 6
Могли использоваться `--add-host`(docker-compose: `extra_hosts`) в развертывании. 
